package Auffgabe3_E;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.regex.Pattern;
import rm.requestResponse.*;

public class PrimeClient {
	private static final String HOSTNAME = "localhost";
	private static final boolean SYNCHRONIZED = false;
	private static final boolean THREAD = false;
	private static final int PORT = 1234;
	private static final long INITIAL_VALUE = (long) 1e17;
	private static final long COUNT = 20;
	private static final String CLIENT_NAME = PrimeClient.class.getName();

	static Random randomGen = new Random();

	private Component communication;
	String hostname;
	int port;
	long initialValue, count;
	static String requestMode;
	int portClient;
	int counter = 0;
	long durchschnitt = 0;
	boolean synchron;
	boolean thread;

	public PrimeClient(String hostname, int port, long initialValue, long count, boolean synchron, boolean thread) {
		this.hostname = hostname;
		this.port = port;
		this.initialValue = initialValue;
		this.count = count;
		this.synchron = synchron;
		this.thread = thread;

		portClient = (randomGen.nextInt(1599) + 4000);

	}

	public void run() throws ClassNotFoundException, IOException {
		communication = new Component();

		for (long i = initialValue; i < initialValue + count; i++) {
			processNumber(i);
		}
		;

	}

	public void processNumber(long value) throws IOException, ClassNotFoundException {
		System.out.println("Client-Port: " + portClient);
		if (synchron == false && thread == true) {

			System.out.print(value + ": ");
			MyThread myThread = new MyThread();
			myThread.start();

			synchronized (PrimeClient.class) {
				try {
					final long timeStart = System.currentTimeMillis();
					communication.send(new Message(hostname, portClient, new Long(value)), port, false);
					String isPrime = (String) communication.receive(portClient, true, true).getContent();
					final long timeEnd = System.currentTimeMillis();

					counter++;

					String[] ausgabe = isPrime.split(Pattern.quote("$"));
					durchschnitt += (timeEnd - timeStart) - Long.parseLong(ausgabe[1]);

					long pundW = Long.parseLong(ausgabe[1]);

					System.out.println(value + ": " + ausgabe[0] + " | c: " + ((timeEnd - timeStart) - pundW) + " ("
							+ durchschnitt / counter + ") ms");

					// System.out.println(value + ": " + (isPrime.booleanValue()
					// ? "prime" : "not prime")+" | c: 0 ("+(timeEnd -
					// timeStart)+") ms");
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
			myThread.setStop(true);

		} else if (synchron == false) {
			final long timeStart = System.currentTimeMillis();
			communication.send(new Message(hostname, portClient, new Long(value)), port, false);
			String isPrime = (String) communication.receive(portClient, true, true).getContent();
			final long timeEnd = System.currentTimeMillis();

			counter++;

			String[] ausgabe = isPrime.split(Pattern.quote("$"));
			durchschnitt += (timeEnd - timeStart) - Long.parseLong(ausgabe[1]);

			long pundW = Long.parseLong(ausgabe[1]);

			System.out.println(value + ": " + ausgabe[0] + " | c: " + ((timeEnd - timeStart) - pundW) + " ("
					+ durchschnitt / counter + ") ms");

			// System.out.println(value + ": " + (isPrime.booleanValue() ?
			// "prime" : "not prime")+" | c: 0 ("+(timeEnd - timeStart)+") ms");
		} else {
			final long timeStart = System.currentTimeMillis();
			communication.send(new Message(hostname, portClient, new Long(value)), port, false);
			String isPrime = null;

			System.out.print(value + ": ");
			boolean received = false;

			while (!received) {
				Message mess = communication.receive(portClient, false, true);
				if (mess != null) {

					isPrime = (String) mess.getContent();
					

					final long timeEnd = System.currentTimeMillis();

					counter++;

					String[] ausgabe = isPrime.split(Pattern.quote("$"));
					durchschnitt += (timeEnd - timeStart) - Long.parseLong(ausgabe[1]);

					long pundW = Long.parseLong(ausgabe[1]);
					System.out.println(ausgabe[0] + " | c: " + ((timeEnd - timeStart) - pundW) + " ("
							+ durchschnitt / counter + ") ms");
					received=true;
				} else {
					System.out.print(".");
				}
				try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();

				}
			}

			// System.out.println(value + ": " + (isPrime.booleanValue() ?
			// "prime" : "not prime")+" | c: 0 ("+(timeEnd - timeStart)+") ms");

		}
	}

	public static void main(String args[]) throws IOException, ClassNotFoundException {
		String hostname = HOSTNAME;
		int port = PORT;
		long initialValue = INITIAL_VALUE;
		long count = COUNT;
		boolean synchron = SYNCHRONIZED;
		boolean thread = THREAD;
		boolean doExit = false;

		String input;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Welcome to " + CLIENT_NAME + "\n");

		while (!doExit) {
			System.out.print("Server hostname [" + hostname + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				hostname = input;

			System.out.print("Server port [" + port + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				port = Integer.parseInt(input);

			System.out.println("Request mode synchronized [j]/[n] > ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				synchron = true;
			else
				synchron = false;
			if (synchron == false) {
				System.out.println("Nebenläufigkeit [j]/[n] > ");
				input = reader.readLine();
				if (input.equals("y") || input.equals("j"))
					thread = true;
				else
					thread = false;
			}

			System.out.print("Prime search initial value [" + initialValue + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				initialValue = Integer.parseInt(input);

			System.out.print("Prime search count [" + count + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				count = Integer.parseInt(input);

			new PrimeClient(hostname, port, initialValue, count, synchron, thread).run();
			System.out.println("Exit [n]> ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				doExit = true;
		}
	}
}
