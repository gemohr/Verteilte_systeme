import java.io.*;

public class MySerializer {
	private MySerializableClass mySerializableClass;
	
	MySerializer(MySerializableClass serializableClass) {
		mySerializableClass=serializableClass;
	}
	
	private String readFilename() throws IOException {
		String filename;
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in )); 
		
		System.out.print("filename> ");
		filename=reader.readLine();
		
		return filename;
	}
	
	public void write(String text) throws IOException {
		mySerializableClass.set(text);
		String filename=readFilename();
		
		BufferedWriter output = null;
        try {
            File file = new File(filename+".txt");
            output = new BufferedWriter(new FileWriter(file));
            output.write(mySerializableClass.toString());
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            if ( output != null ) output.close();
        }
		
	}
	
	public String read() throws IOException, ClassNotFoundException {
		String filename=readFilename();
		
		
		BufferedReader br = new BufferedReader(new FileReader("./"+filename+".txt"));
		try {
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    String everything = sb.toString();
		    mySerializableClass.set(everything);
		} finally {
		    br.close();
		}
		
		
		// Implementierung erforderlich
		// SerialisiSerialisiereere mySerializableClass von Datei
		
		return mySerializableClass.toString();
	}
} 
	