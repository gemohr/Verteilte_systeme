package test;

import java.io.Serializable;

public class MySerializableClass implements Serializable{
	private static final long serialVersionUID=1;
	private int id;
	private String string;
	
	//Objekte die serializable implementieren sind plattformunabhängig.
	private transient MyNonSerializableClass myNonSerializableClass = new MyNonSerializableClass();
	private String idNon;
	
	MySerializableClass() {
		id=1234;
		
	}
	
	public void set(String string) {
		this.string=string;
	}
	
	public String toString() {
		return "id: "+id+"; string: "+string + "; idNon: " + myNonSerializableClass.toString();
	}
} 
	