package test;

import java.io.*;

public class MySerializer {
	private MySerializableClass mySerializableClass;
	
	MySerializer(MySerializableClass serializableClass) {
		mySerializableClass=serializableClass;
	}
	
	private String readFilename() throws IOException {
		String filename;
		BufferedReader reader=new BufferedReader(new InputStreamReader(System.in )); 
		
		System.out.print("filename> ");
		filename=reader.readLine();
		
		return filename;
	}
	
	public void write(String text) throws IOException {
		mySerializableClass.set(text);
		String filename=readFilename();
		
		try {
			      FileOutputStream file = new FileOutputStream("./"+filename+".txt");
			      ObjectOutputStream oos = new ObjectOutputStream(file);
			    oos.writeObject(mySerializableClass);
				oos.close();
		}			    catch(IOException ex){
			    System.err.println("Geht nicht");
			    }
		// Implementierung erforderlich
		// Serialisiere mySerializableClass in Datei
		
	}
	
	public String read() throws IOException, ClassNotFoundException {
		String filename=readFilename();
		
		try{
			      InputStream file = new FileInputStream("./"+filename+".txt");
			      InputStream buffer = new BufferedInputStream(file);
			      ObjectInput input = new ObjectInputStream (buffer);
			      mySerializableClass = (MySerializableClass) input.readObject();
			    }
			    catch(IOException ex){
			      System.err.println("Geht nicht");
			    }
			  
		// Implementierung erforderlich
		// Serialisiere mySerializableClass von Datei
		
		return mySerializableClass.toString();
	}
} 
	