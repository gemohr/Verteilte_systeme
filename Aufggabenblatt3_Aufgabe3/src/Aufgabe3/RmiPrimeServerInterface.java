package Aufgabe3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiPrimeServerInterface extends Remote{

	public boolean isPrime(long candidate )throws RemoteException;
		
	
	
}
