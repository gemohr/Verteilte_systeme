package Aufgabe_c;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RmiPrimeServerInterface extends Remote{

	int isPrimeAsync(long value) throws RemoteException;

	boolean isPrimeSync(long value) throws RemoteException;

	boolean isReady(int index) throws RemoteException;

	boolean getResult(int index) throws RemoteException;
	
}
