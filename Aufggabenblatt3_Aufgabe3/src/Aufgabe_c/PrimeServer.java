package Aufgabe_c;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.logging.*;
import java.rmi.server.*;

import rm.requestResponse.*;

public class PrimeServer implements RmiPrimeServerInterface{
	private final static int PORT = 1234;
	private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());
	private static PrimeServer server;
	private Component communication;
	private int port = PORT;
	private static Map<Integer, PrimeCalcThread> threads = new HashMap<Integer, PrimeCalcThread>();
	ThreadPoolExecutor executor;
	private static boolean isFixed = false;
	static BufferedReader reader;
	PrimeServer(int port) {
		communication = new Component();
		if (port > 0)
			this.port = port;

		// setLogLevel(Level.FINER);
	}

	private boolean primeService(long number) {
		for (long i = 2; i < Math.sqrt(number) + 1; i++) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	void setLogLevel(Level level) {
		for (Handler h : LOGGER.getLogger("").getHandlers())
			h.setLevel(level);
		LOGGER.setLevel(level);
		LOGGER.info("Log level set to " + level);
	}

	void listen() throws RemoteException {
		System.out.println("PrimServer started on port: "+ port );
		if(isFixed){
        	executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        	LOGGER.info("Fixed Threadpool is used");
    	} else {
    		executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        	LOGGER.info("Dynamic Threadpool is used");
    	}
    	
		RmiPrimeServerInterface serverStub = (RmiPrimeServerInterface)UnicastRemoteObject.exportObject(server,0);
		Registry registry = LocateRegistry.createRegistry(port);
		registry.rebind("PrimeServer", serverStub);
	}

	public static void main(String[] args) throws RemoteException {
		int port = 0;
		reader=new BufferedReader(new InputStreamReader(System.in ));
		
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-port":
				try {
					port = Integer.parseInt(args[++i]);
				} catch (NumberFormatException e) {
					LOGGER.severe("port must be an integer, not " + args[i]);
					System.exit(1);
				}
				break;
			default:
				LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
			}
		}
		System.out.println("Should the used ThreadPool be fixed or dynamic? (f/d)");
    	String input;
		try {
			input = reader.readLine();
	    	if(input.equalsIgnoreCase("f")){
	    		isFixed = true;
	    	} 
		} catch (IOException e) {
			e.printStackTrace();
		}
		(server=new PrimeServer(port)).listen();
	//	new PrimeServer(port).listen();
	}

	@Override
	public int isPrimeAsync(long value) throws RemoteException {
		int index = threads.size()+1;
		PrimeCalcThread thread = new PrimeCalcThread(value);
		executor.execute(thread);
		threads.put(index, thread);
		return index;
	}
	
	@Override
	public boolean isPrimeSync(long value) throws RemoteException {
		for (long i = 2; i < Math.sqrt(value) + 1; i++) {
			if (value % i == 0)
				return false;
		}
		return true;
	}

	@Override
	public boolean isReady(int index) throws RemoteException {
		PrimeCalcThread thread = threads.get(index);
		if(thread.isCalculated()){
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean getResult(int index) throws RemoteException {
		PrimeCalcThread thread = threads.get(index);
		LOGGER.info("Clientnummer: "+index);
		if(thread.isResult()){
			return true;
		} else {
			return false;
		}
	}
	

	

}
