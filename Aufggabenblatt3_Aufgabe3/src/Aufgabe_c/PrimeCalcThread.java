package Aufgabe_c;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.logging.Logger;


public class PrimeCalcThread extends Thread implements Serializable {
	private long  value;
	private boolean calculated = false;
	private boolean result;
//	ThreadCounterThread threadCounter;
	private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());

	public PrimeCalcThread(long value
//			,ThreadCounterThread threadCounter
			) {
		this.value = value;
		//this.threadCounter = threadCounter;
//		threadCounter.incrementCounter();
		
	}

	public boolean isCalculated() {
		return calculated;
	}

	public void setCalculated(boolean calculated) {
		this.calculated = calculated;
	}
	
	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
	
	@Override
	public void run() {
		try {
		    LOGGER.info("Calculating result for number "+value+"...");
			result = isPrime(value);
			calculated = true; 
			LOGGER.finer("Result for number "+value+" calculated");
			Thread.sleep(3000);
//			threadCounter.decrementCounter();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	


	public boolean isPrime(long value) throws RemoteException {
		for (long i = 2; i < Math.sqrt(value) + 1; i++) {
			if (value % i == 0)
				return false;
		}
		return true;
	}

}
