package Aufgabe_c;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import rm.requestResponse.*;

public class PrimeClient extends Thread {
	private static final String HOSTNAME = "localhost";
	private static final boolean SYNCHRONIZED = false;
	private static final boolean THREAD = false;
	private static final int PORT = 1234;
	private static final long INITIAL_VALUE = (long) 1e17;
	private static final long COUNT = 20;
	private static final String CLIENT_NAME = PrimeClient.class.getName();
	RmiPrimeServerInterface server;
	private Component communication;
	String hostname;
	int port;
	long initialValue, count;
	boolean synchron;
	boolean thread;
	Registry registry;
	public PrimeClient(Registry registry, String hostname, int port, long initialValue,
			long count, boolean synchron, boolean thread) throws AccessException, RemoteException, NotBoundException {
		this.hostname = hostname;
		this.port = port;
		this.initialValue = initialValue;
		this.count = count;
		this.synchron = synchron;
		this.thread = thread;
		this.registry=registry;
		this.server = (RmiPrimeServerInterface)registry.lookup("PrimeServer");
	}

	public void run() {

		communication = new Component();
		for (long i = initialValue; i < initialValue + count; i++)
			try {
				processNumber(i);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public void processNumber(long value) throws IOException,
			ClassNotFoundException {
		if (synchron == false && thread == true) {
			Boolean isPrime;
			System.out.print(value + ": ");
			MyThread myThread = new MyThread();
			myThread.start();

			synchronized (PrimeClient.class) {
				
					System.out.println(value+" is " + (server.isPrimeSync(value)?"prim":"not prim"));

			}
			myThread.setStop(true);
			
		} else if (synchron == false) {
			System.out.println(value+" is " + (server.isPrimeSync(value)?"prim":"not prim"));

		} else {
			boolean received=false;
			System.out.print(value + ": ");
			int index = server.isPrimeAsync(value);
			do {
				if(server.isReady(index)){
					boolean	isPrime =  server.getResult(index);
					received=true;
					System.out.println((isPrime ? " prime" : " not prime"));
				}else{
				System.out.print(".");
					}
				try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} while (!received);
			
			}

		}
	

	public static void main(String args[]) throws IOException,
			ClassNotFoundException, NotBoundException {
		String hostname = HOSTNAME;
		int port = PORT;
		long initialValue = INITIAL_VALUE;
		long count = COUNT;
		boolean synchron = SYNCHRONIZED;
		boolean thread = THREAD;
		boolean doExit = false;

		String input;
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.println("Welcome to " + CLIENT_NAME + "\n");

		while (!doExit) {
			System.out.print("Server hostname [" + hostname + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				hostname = input;

			System.out.print("Server port [" + port + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				port = Integer.parseInt(input);

			System.out.println("Request mode synchronized [j]/[n] > ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				synchron = true;
			else synchron = false;
			if (synchron == false) {
				System.out.println("Nebenläufigkeit [j]/[n] > ");
				input = reader.readLine();
				if (input.equals("y") || input.equals("j"))
					thread = true;
				else thread = false;
			}
			System.out.print("Prime search initial value [" + initialValue
					+ "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				initialValue = Integer.parseInt(input);

			System.out.print("Prime search count [" + count + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				count = Integer.parseInt(input);

//			if (thread == true) {
//
//				 count = count/2;
//				 //Long teil_2 = initialValue+count;
//				 for(int i=0; i<2;i++){
//				
//				 new PrimeClient(hostname, port, initialValue, count,
//				 synchron, thread).start();
//				 initialValue=initialValue+count;
//				 }
//
//			} else {
//			}
			
			Registry registry = LocateRegistry.getRegistry(hostname,port);
			
				
			
			new PrimeClient(registry, hostname, port, initialValue, count, synchron,
					thread).run();
			System.out.println("Exit [n]> ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				doExit = true;
		}
	}
}
