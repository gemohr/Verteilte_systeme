package Aufgabe_e;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.logging.*;

import rm.requestResponse.*;

public class PrimeServer {
	private final static int PORT = 1234;
	private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());

	private Component communication;
	private int port = PORT;
	String wahl;
	
	private int portClient;
	
	PrimeServer(int port,String wahl) {
		communication = new Component();
		if (port > 0)
			this.port = port;
		this.wahl=wahl;
		// setLogLevel(Level.FINER);
	}

//	private boolean primeService(long number) {
//		for (long i = 2; i < Math.sqrt(number) + 1; i++) {
//			if (number % i == 0)
//				return false;
//		}
//		return true;
//	}

	void setLogLevel(Level level) {
		for (Handler h : Logger.getLogger("").getHandlers())
			h.setLevel(level);
		LOGGER.setLevel(level);
		LOGGER.info("Log level set to " + level);
	}

	void listen() {
		LOGGER.info("Listening on port " + port);
		
		
		while (true) {
//			Long request = null;
			LOGGER.finer("Receiving ...");
			Future<Boolean> obj = null;
			ExecutorService executor = null;
			Message request=null;
			try {
				request = communication.receive(port,true , false);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(wahl.equals("f")){
			 executor = Executors.newFixedThreadPool(10);
			}else if(wahl.equals("c")){
			 executor = Executors.newCachedThreadPool();
			}
			
			LOGGER.finer("Sending ...");
			
				
					
				obj = executor.submit(new MyThreadPool((Long) request.getContent(), communication, request.getPort()));
				//Counter
				System.out.println(executor.toString());
				
//					communication.send(new Message("localhost", port, new Boolean(obj.get().booleanValue())), port,
//							true);
				
				LOGGER.fine(" received.");
				
			//System.out.println(executor.toString());

			executor.shutdown();
			

		

			LOGGER.fine("message sent.");

		}

	}

	public static void main(String[] args) {
		int port = 0;
		String input;
		String wahl="f";
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-port":
				try {
					port = Integer.parseInt(args[++i]);
				} catch (NumberFormatException e) {
					LOGGER.severe("port must be an integer, not " + args[i]);
					System.exit(1);
				}
				break;
			default:
				LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
			}
		}

		System.out.println("Fixed or Cached ThreadPool? [c]/[f] ");
		try {
			input= reader.readLine();
			if(input.equals("c")|| input.equals("C")){
				wahl="c";
			}
		
			
			new PrimeServer(port,wahl).listen();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
