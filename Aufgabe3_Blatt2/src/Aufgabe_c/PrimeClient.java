package Aufgabe_c;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import rm.requestResponse.*;

public class PrimeClient extends Thread {
	private static final String HOSTNAME = "localhost";
	private static final boolean SYNCHRONIZED = false;
	private static final boolean THREAD = false;
	private static final int PORT = 1234;
	private static final long INITIAL_VALUE = (long) 1e17;
	private static final long COUNT = 20;
	private static final String CLIENT_NAME = PrimeClient.class.getName();

	private Component communication;
	String hostname;
	int port;
	long initialValue, count;
	boolean synchron;
	boolean thread;

	public PrimeClient(String hostname, int port, long initialValue,
			long count, boolean synchron, boolean thread) {
		this.hostname = hostname;
		this.port = port;
		this.initialValue = initialValue;
		this.count = count;
		this.synchron = synchron;
		this.thread = thread;
	}

	public void run() {

		communication = new Component();
		for (long i = initialValue; i < initialValue + count; i++)
			try {
				processNumber(i);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public void processNumber(long value) throws IOException,
			ClassNotFoundException {
		if (synchron == false && thread == true) {
			Boolean isPrime;
			System.out.print(value + ": ");
			MyThread myThread = new MyThread();
			myThread.start();
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			synchronized (PrimeClient.class) {
				try {
					communication.send(new Message(hostname, port, new Long(value)),port, false);
					isPrime = (Boolean) communication.receive(port, true, true).getContent();
					System.out.println((isPrime ? " prime" : " not prime"));
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
			myThread.setStop(true);
			
		} else if (synchron == false) {
			communication.send(new Message(hostname, port, new Long(value)),port,
					false);
			Boolean isPrime = (Boolean) communication.receive(port, true, true)
					.getContent();

			System.out.println(value + ": "
					+ (isPrime.booleanValue() ? "prime" : "not prime"));
		} else {
			communication.send(new Message(hostname, port, new Long(value)),port,
					false);
			Boolean isPrime = null;
			System.out.print(value + ": ");

			do {
				try {
					isPrime = (Boolean) communication
							.receive(port, false, true).getContent();

					System.out.print(".");
				} catch (NullPointerException e) {

					System.out.print(".");
				}
			} while (isPrime == null);

			System.out
					.println((isPrime.booleanValue() ? "prime" : "not prime"));

		}
	}

	public static void main(String args[]) throws IOException,
			ClassNotFoundException {
		String hostname = HOSTNAME;
		int port = PORT;
		long initialValue = INITIAL_VALUE;
		long count = COUNT;
		boolean synchron = SYNCHRONIZED;
		boolean thread = THREAD;
		boolean doExit = false;

		String input;
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.println("Welcome to " + CLIENT_NAME + "\n");

		while (!doExit) {
			System.out.print("Server hostname [" + hostname + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				hostname = input;

			System.out.print("Server port [" + port + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				port = Integer.parseInt(input);

			System.out.println("Request mode synchronized [j]/[n] > ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				synchron = true;
			else synchron = false;
			if (synchron == false) {
				System.out.println("Nebenläufigkeit [j]/[n] > ");
				input = reader.readLine();
				if (input.equals("y") || input.equals("j"))
					thread = true;
				else thread = false;
			}
			System.out.print("Prime search initial value [" + initialValue
					+ "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				initialValue = Integer.parseInt(input);

			System.out.print("Prime search count [" + count + "] > ");
			input = reader.readLine();
			if (!input.equals(""))
				count = Integer.parseInt(input);

//			if (thread == true) {
//
//				 count = count/2;
//				 //Long teil_2 = initialValue+count;
//				 for(int i=0; i<2;i++){
//				
//				 new PrimeClient(hostname, port, initialValue, count,
//				 synchron, thread).start();
//				 initialValue=initialValue+count;
//				 }
//
//			} else {
//			}
			new PrimeClient(hostname, port, initialValue, count, synchron,
					thread).run();
			System.out.println("Exit [n]> ");
			input = reader.readLine();
			if (input.equals("y") || input.equals("j"))
				doExit = true;
		}
	}
}
