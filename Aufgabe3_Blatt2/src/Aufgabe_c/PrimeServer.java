package Aufgabe_c;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.logging.*;

import Aufgabe_e.MyThreadPool;
import rm.requestResponse.*;

public class PrimeServer {
	private final static int PORT = 1234;
	private final static Logger LOGGER = Logger.getLogger(PrimeServer.class.getName());

	private Component communication;
	private int port = PORT;

	PrimeServer(int port) {
		communication = new Component();
		if (port > 0)
			this.port = port;

		// setLogLevel(Level.FINER);
	}

	private boolean primeService(long number) {
		for (long i = 2; i < Math.sqrt(number) + 1; i++) {
			if (number % i == 0)
				return false;
		}
		return true;
	}

	void setLogLevel(Level level) {
		for (Handler h : LOGGER.getLogger("").getHandlers())
			h.setLevel(level);
		LOGGER.setLevel(level);
		LOGGER.info("Log level set to " + level);
	}

	void listen() {
		LOGGER.info("Listening on port " + port);
		ExecutorService executor = Executors.newFixedThreadPool(10);
	
		while (true) {
			Long request = null;

			LOGGER.finer("Receiving ...");
			Future<Boolean> obj = null;
			LOGGER.finer("Sending ...");
			try {
				
				while (true) {
					obj = executor
							.submit(new MyThreadPool((Long) communication.receive(port, true, false).getContent(), communication, port));
//					communication.send(new Message("localhost", port, new Boolean(obj.get().booleanValue())), port,
//							true);
					System.out.println(executor.toString());
					LOGGER.fine(" received.");
				}
			} catch (ClassNotFoundException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			
			
			

			

			LOGGER.fine("message sent.");

		}

	}

	public static void main(String[] args) {
		int port = 0;

		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-port":
				try {
					port = Integer.parseInt(args[++i]);
				} catch (NumberFormatException e) {
					LOGGER.severe("port must be an integer, not " + args[i]);
					System.exit(1);
				}
				break;
			default:
				LOGGER.warning("Wrong parameter passed ... '" + args[i] + "'");
			}
		}

		new PrimeServer(port).listen();
	}

}
