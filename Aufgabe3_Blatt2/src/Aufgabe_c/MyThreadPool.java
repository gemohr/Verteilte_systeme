package Aufgabe_c;

import java.util.concurrent.Callable;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

public class MyThreadPool implements Callable<Boolean> {

	private long number;
	private Component communication;
	private int port;

	public MyThreadPool(long number, Component communication, int port) {
		this.number = number;
		this.communication = communication;
		this.port = port;
	}

	@Override
	public Boolean call() throws Exception {
		for (long i = 2; i < Math.sqrt(number) + 1; i++) {
			if (number % i == 0) {
				communication.send(new Message("localhost", port, Boolean.FALSE), port, true);
				return Boolean.FALSE;
			}
		}
		
		communication.send(new Message("localhost", port, Boolean.TRUE), port, true);
		return Boolean.TRUE;
	}
}