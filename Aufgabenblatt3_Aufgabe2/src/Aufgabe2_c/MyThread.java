package Aufgabe2_c;

public class MyThread extends Thread{
	private boolean response_angekommen = false;
	

	protected void setStop(boolean response_angekommen) {
		this.response_angekommen = response_angekommen;
	}

	@Override
	public void run() {
		while (!response_angekommen) {
			System.out.print(".");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
