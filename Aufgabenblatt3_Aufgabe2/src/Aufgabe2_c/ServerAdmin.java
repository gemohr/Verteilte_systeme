package Aufgabe2_c;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerAdmin {
	private static final String CONFIG_FILENAME = "server.conf";

	private Set<ServerConfig> serverConfig = new HashSet<>();

	private int serverAvailableCount = 0;
	private String configFilename = "server.conf";
	private static final Logger LOGGER = Logger.getLogger(ServerAdmin.class.getName());

	public ServerAdmin(String configFilename) throws IOException {
		if (configFilename != "") {
			this.configFilename = configFilename;
		}
		readConfigFile();
	}

	public ServerConfig bind() {
		synchronized (serverConfig) {
			if (serverAvailableCount == 0) {
				try {

					serverConfig.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();

				}
			}
			for (ServerConfig s : serverConfig) {

				if (s.isAvailable()) {
					serverAvailableCount -= 1;
					s.bind();
					return s;
				}
			}
		}

		try {
			throw new Exception();
		} catch (Exception e) {
			LOGGER.severe("Couldn't bind");
		}
		return null;
	}

	public void release(ServerConfig config) {

		synchronized (serverConfig) {

			config.release();
			serverAvailableCount += 1;
			serverConfig.notify();
		}
	}

	public void setLogLevel(Level level) {
		Handler[] handlers;

		int j = (handlers = Logger.getLogger("").getHandlers()).length;
		for (int i = 0; i < j; i++) {
			Handler h = handlers[i];
			h.setLevel(level);
		}
		LOGGER.setLevel(level);
		LOGGER.info("Log level set to " + level);
	}

	private void readConfigFile() throws IOException {
		FileReader fileReader = new FileReader(configFilename);
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		fileReader = new FileReader(configFilename);
		bufferedReader = new BufferedReader(fileReader);
		String input;
		while ((input = bufferedReader.readLine()) != null) {
			String[] words = input.split(" ");
			serverConfig.add(new ServerConfig(words[0], Integer.parseInt(words[1]), Integer.parseInt(words[2])));

		}
		serverAvailableCount = serverConfig.size();
		int i = 0;
		for (ServerConfig s : serverConfig) {
			i++;
			LOGGER.info("Server " + i + ": " + s.toString());
		}

		bufferedReader.close();
	}

}
