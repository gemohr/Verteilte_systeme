package Aufgabe2_c;

public class ServerConfig {
	private String hostname;
	private int receivePort;
	private int sendPort;
	private boolean available;

	public ServerConfig(String hostname, int receivePort, int sendPort) {
		this.hostname = hostname;
		this.receivePort = receivePort;
		this.sendPort = sendPort;
		this.available = true;
	}

	public String getHostname() {
		return this.hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getReceivePort() {
		return this.receivePort;
	}

	public void setReceivePort(int port) {
		this.receivePort = port;
	}

	public int getSendPort() {
		return this.sendPort;
	}

	public void setSendPort(int port) {
		this.sendPort = port;
	}

	public String toString() {
		return "hostname: " + hostname + "; receive port: " + receivePort + "; send port: " + sendPort;
	}

	void bind() {
		available = false;
	}

	void release() {
		System.out.println("release: " + available);
		available = true;
	}

	boolean isAvailable() {
		System.out.println("isAvailable " + available);
		return available;
	}
}
