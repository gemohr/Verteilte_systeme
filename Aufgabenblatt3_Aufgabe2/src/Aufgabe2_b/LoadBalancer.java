package Aufgabe2_b;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

import rm.requestResponse.Component;
import rm.requestResponse.Message;
import rm.serverAdmin.ServerAdmin;
import rm.serverAdmin.ServerConfig;

public class LoadBalancer {
	 private static ServerAdmin serverAdmin;
	    private static final int PORT = 1234;
	    private static final Logger LOGGER = Logger.getLogger(LoadBalancer.class.getName());
	    private static final int THREADPOOL_SIZE = 5;
	    private static final Properties SERVERCONFIG = new Properties();
	    private static final List<String> PORTS = new ArrayList<>();
	    private final Component communication;
	    private static int port = PORT;
	    private static Message msg;
		ThreadPoolExecutor executor;
		
		
		
		private class arbeiter implements Runnable {
			
			private final Message mess;
			
			public arbeiter(Message mess){
				this.mess=mess;
			}
			
			
			public void run() {
				
				ServerConfig serverConfig = serverAdmin.bind();
				try {
					communication.send(mess, serverConfig.getReceivePort(), true);
					LOGGER.info("Fertig gesendet.....");
					serverAdmin.release(serverConfig);
					LOGGER.info("Server wieder freigegeben.....");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
		
		
		
		public LoadBalancer(int port) {
			this.executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(THREADPOOL_SIZE);
			communication = new Component();
			if(port>0) this.port=port;
		}

		private void listen() {
			
			try {
				while(true){
					
				Message mess = communication.receive(port, true, false);
				
				Runnable arbeit = new arbeiter(mess);
				executor.execute(arbeit);
				
				}
				
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}



		public static void main(String[] args) {
			  try {
				serverAdmin = new ServerAdmin("./cluster.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		        new LoadBalancer(port).listen();
		}





		
}
