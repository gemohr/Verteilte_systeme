package Aufgabe2_b;

import java.io.IOException;
import java.util.logging.Logger;

import rm.requestResponse.Component;
import rm.requestResponse.Message;

public class TaskThread implements Runnable {

	private Component communication;
	private Message msg;
	public static int counter;
	private Counter c;
	public long p;
	public static long pdurch;
	public long wstart;
	public static long wdurch;
	public long start;
	public int sendPort;
	
	public TaskThread(Component communication, Message msg, Counter c,long wstart, int sendPort) {
		this.c = c;
		this.communication = communication;
		this.msg = msg;
		c.incrementCounter();
		this.wstart=wstart;
		this.sendPort=sendPort;
	}

	private final static Logger LOGGER = Logger.getLogger(TaskThread.class.getName());

	public void run() {
		// TODO Auto-generated method stub

		LOGGER.finer("Sending ...");
		try {

			Long request = (Long) msg.getContent();
			// sende an Client zur�ck
			// communication.send(new Message("localhost", 0, new Boolean(
			// primeService(request.longValue()))), msg.getPort(), true);
			
			start = System.currentTimeMillis()-wstart;
			
			
			long ptimeStart = System.currentTimeMillis();
			String prime = primeService(request.longValue()) ? "prime" : "not prime";
			
			p = System.currentTimeMillis() - ptimeStart;
			pdurch += p;
			wdurch += start;
			System.err.println("p und w: " +(p + start));
			System.err.println("w: "+start);
			System.err.println("p: "+p);
			System.err.println("pdurch: "+pdurch);
			System.err.println("wdurch: "+wdurch);
			System.err.println("counter: "+c.countergesamt.intValue());
			
			
			communication.send(new Message("localhost", 1234, prime + " | p: " + p + " ("
							+ pdurch / c.countergesamt.intValue() + ") ms | w: " +start + " ("
							+ wdurch / c.countergesamt.intValue() + ") ms $" + (p + start)+ "$"+start+"$"),
					msg.getPort(), true);

			c.decrementCounter();

		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.fine("message sent.");

		}

	}

	private boolean primeService(long number) {
		
		for (long i = 2; i < Math.sqrt(number) + 1; i++) {
			if (number % i == 0) {
				return false;
			}
		}
		return true;
	}

}
