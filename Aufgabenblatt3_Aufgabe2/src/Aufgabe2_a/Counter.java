package Aufgabe2_a;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter extends Thread {
	public static AtomicInteger counter = new AtomicInteger();
	public static AtomicInteger countergesamt = new AtomicInteger();
	public long w;
	public static long wdurch;
	
	private Object monitor = new Object();

	public void incrementCounter() {
		synchronized (monitor) {
			countergesamt.getAndIncrement();
			counter.getAndIncrement();
			monitor.notify();
		}
	}

	public void decrementCounter() {
		synchronized (monitor) {
			counter.getAndDecrement();
			monitor.notify();
		}
	}

	public void run() {
		long wtimeStart = System.currentTimeMillis();
		
		while (true) {
			synchronized (monitor) {
				w = System.currentTimeMillis()-wtimeStart;
				
				try {
					monitor.wait();
					System.out.println("Threads aktiv: " + counter.get());

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
				wdurch+=w;
			}
		}
	}

}