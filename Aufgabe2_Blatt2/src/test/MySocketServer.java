package test;

import java.io.*;
import java.net.*;
 
public class MySocketServer {
	private ServerSocket socket;
	private int port;
	private String filename;
	PrintWriter pw = null;
	
	public MySocketServer(int port, String filename) 
					throws IOException {
		this.port=port;
		this.filename=filename;
		socket=new ServerSocket(port);
		
	}

	public void listen() {
		while(true) {
			try {
				pw = new PrintWriter(new BufferedWriter(new FileWriter(filename,true)),true);
				pw.write("Server: listening on port "+port+"\n");
				System.out.println("Server: listening on port "+port);
				Socket incomingConnection=socket.accept();
				System.err.println(incomingConnection.getRemoteSocketAddress());
				pw.write(incomingConnection.getInetAddress()+":"+incomingConnection.getPort()+"\n");
				System.out.println(incomingConnection.getInetAddress()+":"+incomingConnection.getPort());
				MySocketServerConnection connection=
					new MySocketServerConnection(incomingConnection,filename);
				connection.start();
			pw.flush();
			pw.close();
			}
			catch(IOException e) {
				e.printStackTrace();
			}
		}
	}
}
