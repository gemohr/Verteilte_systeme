 package test;

import java.io.IOException;



public class ServerMain {
	private static final int port=1234;
	private static MySocketServer server;
	private static String fileName = "log.txt";
	
	public static void main(String args[]) {
		try {
			server=new MySocketServer(port,fileName);
			server.listen();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
