package test;

import java.io.*;
import java.net.*;
import java.util.regex.Pattern;

public class MySocketServerConnection extends Thread {
	private Socket socket;
	private DataInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;
	private PrintWriter pw = null;
	private String url;

	public MySocketServerConnection(Socket socket, String filename)
			throws IOException {
		this.socket = socket;
		pw = new PrintWriter(
				new BufferedWriter(new FileWriter(filename, true)), true);

		objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

		BufferedReader rd = new BufferedReader(new InputStreamReader(
				socket.getInputStream()));

		url = rd.readLine();

		System.err.println(url);

		pw.write("Server: incoming connection accepted.\n");
		System.out.println("Server: incoming connection accepted.");

	}

	public void run() {
		pw.write("Server: waiting for message ...\n-----------------------------------------\n\n");
		System.out.println("Server: waiting for message ...");
		BufferedReader r;

		if (url != null) {
			url = (String) url.subSequence(4, url.length() - 9);
		} else {
			url = "/NewFile.html";
		}

		if (url.equals("/")) {
			url = "/NewFile.html";
		}

		System.out.println(url);
		String line = "";
		try {
			StringBuilder stringBuilder = new StringBuilder();

			r = new BufferedReader(new FileReader("Z:\\neu\\Aufgabe2_Blatt2\\src\\test\\"
					+ url));

			try {
				while ((line = r.readLine()) != null) {

					stringBuilder.append(line);

				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String zeile = stringBuilder.toString();

			String string = "HTTP/1.1 200 OK \r\n\r\n" + zeile;

			System.out.println(string);
			
			pw.flush();
			pw.close();
			
				objectOutputStream.writeObject(string);
				socket.close();
			

		} catch (IOException e) {
			try {
				objectOutputStream
						.writeObject("HTTP/1.1 404 OK \r\n\r\n <!DOCTYPE html><html></html>");
				socket.close();
				System.out.println("erster catch");
			} catch (IOException e1) {
				
				
				e1.printStackTrace();
			}
		}
	}
}
